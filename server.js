"use strict";

const express = require("express");
const app = express();
const HttpStatus = require("http-status-codes");

const { PORT } = require("./app/config");

require("./app/config/express").init(app);
require("./app/config/route").init(app);
require("./app/config/mongoose").init(app);

app.all("*", function (req, res, next) {
  res.status(HttpStatus.BAD_REQUEST).json({
    status: "fail",
    message: `Cant find ${req.url} on this server`,
  });
});

app.use(function (err, req, res, next) {
  return res.status(HttpStatus.BAD_REQUEST).json({
    status: "error",
    message: (err && err.message) || "Default message",
  });
});

app.listen(PORT, function () {
  console.log(`API on port ${PORT}`);
});
