"use strict";

const Car = require("../models/carModel");

module.exports = {
  createCar,
  getCars,
  getCarById,
  updateCar,
  deleteCar,
};

function createCar(req, res, next) {
  const car = new Car(req.body);

  car.save(function (err, result) {
    if (err) {
      return next(err);
    }

    req.resources.car = result;
    return next();
  });
}

function getCars(req, res, next) {
  Car.find()
    .populate("user", "name email")
    .sort({ brand: 1 })
    .exec(function (err, result) {
      if (err) {
        // TODO: add proper errorhandling
        console.log("Unable to return data: " + err);
        return res.send("Unable to return data!");
      }

      req.resources.car = result;

      return next();
    });
}

function getCarById(req, res, next) {
  const { id } = req.params;

  Car.find({ _id: id }, function (err, result) {
    if (err) {
      return res.send("Unable to return data!");
    }

    req.resources.car = result;
    return next();
  });
}

function updateCar(req, res, next) {
  const { id } = req.params;
  const updateData = req.body;

  Car.findOneAndUpdate({ _id: id }, updateData, function (err, result) {
    if (err) {
      return next({ message: "Unable to update data!" });
    }

    req.resources.car = result;
    return next();
  });
}

function deleteCar(req, res, next) {
  Car.deleteOne({ _id: req.params.id }, function (err, result) {
    if (err) {
      return res.send("Unable to delete the item!");
    }

    req.resources.car = result;
    return next();
  });
}
