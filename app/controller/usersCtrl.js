"use strict";

const User = require("../models/userModel");

module.exports = {
  createUsers,
  getUsers,
  getUserById,
  updateUser,
  deleteUser,
};

function createUsers(req, res, next) {
  const user = new User(req.body);

  user.save(function (err, result) {
    if (err) {
      return next(err);
    }

    req.resources.user = result;
    return next();
  });
}

function getUsers(req, res, next) {
  User.find(function (err, result) {
    if (err) {
      return res.send("Unable to return data");
    }

    req.resources.user = result;
    return next();
  });
}

function getUserById(req, res, next) {
  const { id } = req.params;

  User.find({ _id: id }, function (err, result) {
    if (err) {
      return res.send("Unable to return data");
    }

    req.resources.user = result;
    return next();
  });
}

function updateUser(req, res, next) {
  const { id } = req.params;
  const updateData = req.body;

  User.findOneAndUpdate({ _id: id }, updateData, function (err, result) {
    if (err) {
      return next({ message: "Unable to update data" });
    }

    req.resources.user = result;
    return next();
  });
}

function deleteUser(req, res, next) {
  User.deleteOne({ _id: req.params.id }, function (err, result) {
    if (err) {
      return res.send("Unable to delete item!");
    }

    req.resources.user = result;
    return next();
  });
}
