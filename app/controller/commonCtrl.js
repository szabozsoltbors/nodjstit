"use strict";

const HttpStatus = require("http-status-codes");

module.exports = {
  responseToJSON,
  isAdmin,
};

function responseToJSON(prop) {
  return function (req, res, next) {
    res.json(req.resources[prop]);
  };
}

function isAdmin(req, res, next) {
  // TODO: add proper admin/user handler
  if (false) {
    return next();
  }

  return res.status(HttpStatus.UNAUTHORIZED).json({
    status: "error",
    message: "You are not autorized to access this page!",
  });
}
