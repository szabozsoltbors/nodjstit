"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
    required: [true, "Name is required"],
    unique: false,
  },
  email: {
    type: String,
    required: true,
    unique: false,
  },
  age: {
    type: Number,
  },
});

module.exports = mongoose.model("User", userSchema, "users");
