"use strict";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

const carSchema = new Schema({
  brand: {
    type: String,
    required: [true, "Brand is required"],
    unique: false,
  },
  color: {
    type: String,
    required: true,
    unique: false,
  },
  user: {
    type: ObjectId,
    ref: "User",
    required: true,
  },
});

module.exports = mongoose.model("Car", carSchema, "cars");
