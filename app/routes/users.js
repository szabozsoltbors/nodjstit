"use strict";

const express = require("express");
const router = express.Router();
const { pathToDestination } = require("../config");

const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, pathToDestination);
  },
  filename: function (req, file, cb) {
    cb(null, `${Date.now()}-${file.originalname}`);
  },
});

const upload = multer({ storage: storage });

const usersCtrl = require("../controller/usersCtrl");
const commonCtrl = require("../controller/commonCtrl");

router.post("/upload-profile", upload.single("profilePicture"), function (
  req,
  res,
  next
) {
  console.log("FILE", req.file);
  console.log("FILES", req.files);
  res.send("Uploaded");
});

router.post(
  "/upload-multi-profile",
  upload.array("profilePicture", 2),
  function (req, res, next) {
    console.log("FILE", req.file);
    console.log("FILES", req.files);
    res.send("Uploaded");
  }
);

router.get("/download-image", function (req, res, next) {
  const pathToFile = `${pathToDestination}/1597830716659-Screenshot from 2020-04-20 16-13-43.png`;
  res.download(pathToFile);
});

router.post("/users", usersCtrl.createUsers, commonCtrl.responseToJSON("user"));

router.get("/users", usersCtrl.getUsers, commonCtrl.responseToJSON("user"));

router.get(
  "/users/:id",
  usersCtrl.getUserById,
  commonCtrl.responseToJSON("user")
);

router.put(
  "/users/:id",
  usersCtrl.updateUser,
  commonCtrl.responseToJSON("user")
);

router.delete(
  "/users/:id",
  usersCtrl.getUserById,
  usersCtrl.deleteUser,
  commonCtrl.responseToJSON("user")
);

module.exports = router;
