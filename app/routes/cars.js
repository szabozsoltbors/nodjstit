"use strict";

const express = require("express");
const router = express.Router();
const carsCtrl = require("../controller/carsCtrl");
const commonCtrl = require("../controller/commonCtrl");

router.post(
  "/cars",
  commonCtrl.isAdmin,
  carsCtrl.createCar,
  commonCtrl.responseToJSON("car")
);

router.get(
  "/cars",
  commonCtrl.isAdmin,
  carsCtrl.getCars,
  commonCtrl.responseToJSON("car")
);
router.get(
  "/cars/:id",
  commonCtrl.isAdmin,
  carsCtrl.getCarById,
  commonCtrl.responseToJSON("car")
);

router.put(
  "/cars/:id",
  commonCtrl.isAdmin,
  carsCtrl.updateCar,
  commonCtrl.responseToJSON("car")
);

router.delete(
  "/cars/:id",
  commonCtrl.isAdmin,
  carsCtrl.getCarById,
  carsCtrl.deleteCar,
  commonCtrl.responseToJSON("car")
);

module.exports = router;
